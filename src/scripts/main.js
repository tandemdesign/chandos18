const menu = document.querySelector('.menu-icon');
const head = document.querySelector('header');
const nav = document.querySelector('nav');
const navUl = document.querySelector('nav.main > ul');
const navLi = document.querySelectorAll('li.has-subnav');
const navItems = document.querySelectorAll('nav.main ul li');

const projectFilter = document.querySelectorAll('ul.locations li input');
const teamFilter = document.querySelectorAll('ul.departments li input');

const content = document.querySelector('.results');
const searchError = document.querySelector('.search-error');
const searchButton = document.querySelector('.search-open');
const searchIcon = document.querySelector('.search-icon');

let projects = document.querySelectorAll('a.project');
let items = document.querySelectorAll('.results a');
let checkedBoxesCount = 0;
const sideBar = document.querySelector('.sidebar');

const officeInfoBoxes = document.querySelectorAll('.contact-single');
const officeLinks = document.querySelectorAll('.office-link');
const officeCloseBoxes = document.querySelectorAll('.contact-single .close');

const scrollBtn = document.querySelector('.scroll-down');

const slider = document.querySelector('.slider');

const eventBtn = document.querySelector('.event-btn');

const torontoEvent = document.querySelector('#torontoEvent');
const calgaryEvent = document.querySelector('#calgaryEvent');

const pageTitle = document.querySelector('.big-img h1 span');

//all main navigation/menu shit
menu.addEventListener('click', toggleMenu);
navItems.forEach(item => item.addEventListener('click', toggleNav));

//click arrow on big hero image, smooth scroll to main content
if(scrollBtn){
  scrollBtn.addEventListener('click', function(e){
    e.preventDefault();
    const target = document.querySelector(this.hash);
    //console.log(target);  
    target.scrollIntoView({
      behavior: 'smooth'
    });
    
  });
}

//console.log(pageTitle.firstChild.length);

//make small title when headline too big

if(pageTitle){
  if(pageTitle.firstChild.length > 20){
    document.querySelector('.big-img h1').classList.add('long');
  }
}



//change search button icon when clicked
if(searchButton){
  searchButton.addEventListener('click', function(){
    if(this.classList.contains('opened')){
      this.classList.remove('opened');
      searchIcon.innerHTML = "<use xlink:href='#iconSearch'></use>";
    } else {
      this.classList.add('opened');
      searchIcon.innerHTML = "<use xlink:href='#iconClose'></use>";
    }
  });
}

/*
officeLinks.forEach(office => office.addEventListener('click', function(e){
  let thisOffice;
  if(this.hash == null){
    thisOffice = document.querySelector(this.href.baseVal);
  } else {
    thisOffice = document.querySelector(this.hash);
  }
  e.preventDefault();
  thisOffice.classList.add('active');
  thisOffice.scrollIntoView({
    behavior: 'smooth'
  });
  
}));
*/
/*
officeCloseBoxes.forEach(close => close.addEventListener('click', function(e){
  e.preventDefault();
  let thisParent = this.offsetParent;
  thisParent.classList.remove('active');
}));
*/

//if team page, check if there is a hash, if hash, open that team member (from clicking on name in contact page)
if(document.getElementById("teamPage")){
  if(window.location.hash){
    console.log(window.location.hash);
    $(window.location.hash).fancybox().trigger('click');
  }
}
 

$('.fancybox').fancybox();

//contact page popups
$('.office-link').fancybox({
  type: 'inline'
})



//fix side bar when scrolling
if(sideBar != null){
  window.addEventListener('scroll', fixSidebar);
}

projectFilter.forEach(box => box.addEventListener('change', filterProjects));
teamFilter.forEach(box => box.addEventListener('change', filterTeam));

function fixSidebar(){
  const footer = document.querySelector('footer');
  const sbContent = document.querySelector('.sidebar .content');
  //window.scrollY + window.innerHeight < footer.offsetTop 
  if(window.scrollY > sideBar.offsetTop){
    //console.log('scrolled');
    sideBar.classList.add('fixed');
  } else {
    sideBar.classList.remove('fixed');
  }  

}


//project filtering 
function filterProjects(){
    let checkedBoxes = document.querySelectorAll('ul.locations li input:checked'); //find all checked boxes
    let result = ''; //where we will store what boxes are checked

    //remove active class from all items
    items.forEach(i => {
      i.classList.remove('active');
    });

    //for each checked box, add the class associated with it to "result" string
    checkedBoxes.forEach(checked => {
      let value = checked.getAttribute('name');
      result += '.'+value;
      
    });
    //console.log(result);
    //console.log(document.querySelectorAll(result));
    //if result has stuff in it, go through items and add active class to whatever has a class in result string
    if(result != ''){
      let filteredResults = document.querySelectorAll(result);
      //console.log(filteredResults);
      filteredResults.forEach(f => {
        f.classList.add('active');
      });
      if(filteredResults.length === 0){
        //console.log('No Results');
        searchError.classList.add('on');
      } else {
        searchError.classList.remove('on');
      }
    } else {
      //if no checkboxes are checked, just show everything
      items.forEach(i => {
        i.classList.add('active');
      });
    }

    /*
    if(this.checked){
      checkedBoxes++;
      //content.classList.add(city);
      projects.forEach(function(p){
        if(p.classList.contains(filterName)){
          p.classList.add('active');
        }
      });

    } else {
      checkedBoxes--;
      //content.classList.remove(city);
      projects.forEach(function(p){
        if(p.classList.contains(filterName)){
          p.classList.remove('active');
        }
      });
    }
    */

    /*
    if(checkedBoxes > 0){
      content.classList.add('filtered');
    } else {
      content.classList.remove('filtered');
    } 
    */

  
}

//people page team filter
function filterTeam(){
  let checkedBoxes = document.querySelectorAll('ul.departments li input:checked');
  let filterName = this.getAttribute('name');
  if(this.checked){
      checkedBoxesCount++;
      //content.classList.add(city);
      items.forEach(function(p){
        if(p.classList.contains(filterName)){
          p.classList.add('active');
        }
      });

    } else {
      checkedBoxesCount--;
      //content.classList.remove(city);
      items.forEach(function(p){
        if(p.classList.contains(filterName)){
          p.classList.remove('active');
        }
      });
    }
   
    if(checkedBoxesCount > 0){
      content.classList.add('filtered');
    } else {
      content.classList.remove('filtered');
    } 
  
}

      

//open/close menu
function toggleMenu(){
  head.classList.toggle('nav-open');
  nav.classList.toggle('is-open');
  this.classList.toggle('nav-open');
  navLi.forEach(li => li.classList.remove('active'));
  navUl.classList.remove('faded');
}

function toggleNav(){
  if(this.classList.contains('has-subnav')){
    //open subnav if has subnav
    toggleSubNav(this);
  } else {
    //closes menu
    closeMenu();
  }
}

// close menu
function closeMenu(){
  head.classList.remove('nav-open');
  nav.classList.remove('is-open');
  menu.classList.remove('nav-open');
  navLi.forEach(li => li.classList.remove('active'));
}

//open sub nav and fade un-clicked 
function toggleSubNav(el){
  if(el.classList.contains('active')){
    el.classList.remove('active');
    el.parentElement.classList.remove('faded');
  } else {
    navLi.forEach(li => li.classList.remove('active'));
    el.classList.add('active');
    el.parentElement.classList.add('faded');
  }
}

//homepage slider, it DID exist at one point, keeping this here in case client changes mind
function switchSlide(){
  //console.log('switch');
  const activeSlide = document.querySelector('.slider div.active');
  const playedSlide = document.querySelectorAll('.played');

  activeSlide.classList.add('played');

  let next = activeSlide.nextElementSibling;

  if(next == null){
    next = slider.children[0];
  }

  next.classList.add('active');
  playedSlide.forEach(slide => slide.classList.remove('active'));

  setTimeout(function(){
    playedSlide.forEach(slide => slide.classList.remove('played'));
  }, 2000);


}

if(slider != null){
  setInterval(switchSlide, 5000);
}
// analytics click events on "GET TICKETS" links 
if(torontoEvent != null){
  torontoEvent.addEventListener('click', function(){
    gtag('event', 'click', {
        'send_to': 'UA-29752139-1',
        'event_category': 'Links',
        'event_label': 'Event - Toronto'
  
    });
  });

}

if(calgaryEvent != null){
  calgaryEvent.addEventListener('click', function(){
    gtag('event', 'click', {
        'send_to': 'UA-29752139-1',
        'event_category': 'Links',
        'event_label': 'Event - Calgary'
  
    });
  });
}

