//wistia BG video code

const videoExists = document.querySelector(".background-video");

if(typeof(videoExists) != 'undefined' && videoExists != null){
  var fullScreenVideo = fullScreenVideo || {};
  
  fullScreenVideo = {
      name: 'fullScreenVideo',
      backgroundvideo: '9z4lh3x7j6',
      backgroundideoDiv: '#wistia_9z4lh3x7j6',
      

      embedVideo: function()
      {
        var videoOptions = {
          volume:0
        };
    
        // Add the crop fill plugin to the videoOptions
        Wistia.obj.merge(videoOptions, {
          plugin: {
            cropFill: {
              src: "//fast.wistia.com/labs/crop-fill/plugin.js"
            }
          }
        });
  
        // Video in the background
        wistiaEmbed = Wistia.embed(fullScreenVideo.backgroundvideo, videoOptions);
        
        wistiaEmbed.bind("play", function(){
          wistiaEmbed.pause();
          wistiaEmbed.time(0);
          //$(fullScreenVideo.backgroundideoDiv).css('visibility', 'visible');
          wistiaEmbed.play();
          return this.unbind;
        });
      },
     
     
       
  }
  
  fullScreenVideo.embedVideo();
}