
<a class="event-btn fancybox" href="#event" style="position:absolute;z-index:34934;right:0" ></a>
<div id="event" class="event-popup" style="display:none;">
	<div class="container">
		<img class="banner" src="<?=$templates?>img/banner_event-iwp2019.jpg" alt="Industry With Purpose banner">
		<div class="info">
			<h2>Can your company do well <span>By doing good?</span></h2>
			<h3>Toronto <span>•</span> Edmonton <span>•</span> Calgary <span>•</span> Vancouver</h3>
			<p>Join Craig Kielburger and Afdhel Aziz for a dynamic morning of learning and insight. The bottom line? Your business will be more profitable by having purpose and doing good.</p>
				<p><a class="btn" href="<?=$pages->get('/what-we-do/thought-leadership/')->url?>">Learn More</a></p>
		</div>
	</div>
</div>