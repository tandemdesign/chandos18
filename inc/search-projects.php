<div class="search-container">
	<input type="checkbox" class="search-reveal" id="search" name="search">
	<div class="search-menu">
		<h2>Filter By</h2>
		<div>
			<h3>Type</h3>
			<form id="typeForm" method="GET">
				<ul class="locations">
					
				<li><label for="featured"><input id="featured" type="checkbox" name="featured" checked /><span class="check"></span> Featured</label></li>
				<?php 
				foreach($page->children as $type){
					
					echo "<li>
					<label for='{$type->name}'> 
					<input id='{$type->name}' type='checkbox' name='{$type->name}' />
					<span class='check'></span>  
					{$type->title}</label>
					</li>";
						
				
				
				}?>
		

				</ul>
			
			<!--<input type="submit" value="submit">-->
			</form>
		</div>
		<div>
			<h3>Served From</h3>
			<form id="locationForm" method="GET">
				<ul class="locations">
					<?php 
					$locations = $fieldtypes->get('FieldtypeOptions')->getOptions('served_from'); 	
					foreach($locations as $l){
						$projects = $pages->find("template=project-single,served_from={$l->title}");
						if(count($projects)){
							$location = strtolower($l->title);
							$name = preg_replace("/[\s_]/", "-", $location);
							echo "<li>
							<label for='{$name}'> 
							<input id='{$name}' type='checkbox' name='{$name}' />
							<span class='check'></span>  
							{$l->title}</label>
							</li>";
						}
						
					}?>

				</ul>
			
			<!--<input type="submit" value="submit">-->
			</form>
		</div>
		<div>
			<h3>Services</h3>
			<form id="locationForm" method="GET">
				<ul class="locations">
					<?php
					$services = $fieldtypes->get('FieldtypeOptions')->getOptions('services'); 	
					foreach($services as $s){
						$projects = $pages->find("template=project-single,services={$s->title}");
						if(count($projects)){
							$name = "serv{$s->id}";
							echo "<li>
							<label for='{$name}'> 
							<input id='{$name}' type='checkbox' name='{$name}' />
							<span class='check'></span>  
							{$s->title}</label>
							</li>";
						}
						
					}?>
				</ul>
			
			<!--<input type="submit" value="submit">-->
			</form>
		</div>
		
		
	</div><!--search-menu-->
					
					
</div><!--search-container-->