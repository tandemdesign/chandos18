<div class="search-container">
	<input type="checkbox" class="search-reveal" id="search" name="search">
	<div class="search-menu">
		<h2>Filter By</h2>
		<div>
			<h3>Department</h3>
			<form id="typeForm" method="GET">
				<ul class="departments">
				<?php 
				$departments = $fieldtypes->get('FieldtypeOptions')->getOptions('department'); 
				foreach($departments as $dept){
					
					echo "<li>
					<label for='{$dept->value}'> 
					<input id='{$dept->value}' type='checkbox' name='{$dept->value}'/>
					<span class='check'></span>  
					{$dept->title}</label>
					</li>";
						
				
				
				}?>
		

				</ul>
			
			<!--<input type="submit" value="submit">-->
			</form>
		</div>
		
	</div><!--search-menu-->
					
					
</div><!--search-container-->