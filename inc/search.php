<div class="search-container">
	<input type="checkbox" class="search-reveal" id="search" name="search">
	<div class="search-menu">
		<h2>Filter By</h2>
		<div>
			<h3>Type</h3>
			<form id="typeForm" method="GET">
				<ul class="locations">
					
				<li><label for="featured"><input id="featured" type="checkbox" name="featured" checked /><span class="check"></span> Featured</label></li>
				<?php 
				//only display locations available in this category
				foreach($page->children as $type){
					
					echo "<li>
					<label for='{$type->name}'> 
					<input id='{$type->name}' type='checkbox' name='{$type->name}' />
					<span class='check'></span>  
					{$type->title}</label>
					</li>";
						
				
				
				}?>
		

				</ul>
			
			<!--<input type="submit" value="submit">-->
			</form>
		</div>
		<div>
			<h3>Location</h3>
			<form id="locationForm" method="GET">
				<ul class="locations">
					
				
				<?php 
				//only display locations available in this category
				foreach($locations as $location){			
						echo "<li>
						<label for='{$location->name}'> 
						<input id='{$location->name}' type='checkbox' name='{$location->name}' />
						<span class='check'></span>  
						{$location->title}</label>
						</li>";

				}?>
		

				</ul>
			
			<!--<input type="submit" value="submit">-->
			</form>
		</div>
		
		<div>
			<h3>Service Type</h3>
			<form id="serviceForm" method="GET">
				<ul class="category">
					<li>
						<label for='ipda'> 
							<input id='ipda' type='checkbox' name='ipda' />
							<span class='check'></span>  
							IPDA Integrated Project Delivery
						</label>
					</li>
					<li>
						<label for='designbuild'> 
							<input id='designbuild' type='checkbox' name='designbuild' />
							<span class='check'></span>  
							Design Build
						</label>
					</li>
					<li>
						<label for='ssg'> 
							<input id='ssg' type='checkbox' name='ssg' />
							<span class='check'></span>  
							Special Services Group
						</label>
					</li>
					<li>
						<label for='leed'> 
							<input id='leed' type='checkbox' name='leed' />
							<span class='check'></span>  
							Sustainability / LEED or Zero
						</label>
					</li>
				
		

				</ul>
		</div>
	</div><!--search-menu-->
					
					
</div><!--search-container-->