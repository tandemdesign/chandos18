<ul class="social">
	<li><a title="Like Chandos on Facebook" target="_blank" href="https://www.facebook.com/ChandosConstruction/"><svg><use xlink:href="#iconFacebook"></use></svg></a></li>
	<li><a title="Follow Chandos on Twitter" target="_blank" href="https://twitter.com/ChandosLTD"><svg><use xlink:href="#iconTwitter"></use></svg></a></li>
	<li><a title="Follow Chandos on LinkedIn" target="_blank" href="https://ca.linkedin.com/company/chandos"><svg><use xlink:href="#iconLinkedIn"></use></svg></a></li>
	<li><a title="Visit the Chandos Vimeo gallery" target="_blank" href="https://vimeo.com/chandos"><svg><use xlink:href="#iconVimeo"></use></svg></a></li>
</ul>
