const gulp = require('gulp');
const sass = require('gulp-sass');
const rename = require('gulp-rename');
const autoprefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify-es').default;
const imagemin = require('gulp-imagemin');
const sftp = require('gulp-sftp-up4');

const paths = {
	styles: {
		src: 'src/styles/**/*.scss',
		dest: 'css/'
	},
	scripts: {
		src: 'src/scripts/**/*.js',
		dest: 'js/'
	},
	images: {
		src: 'src/img/**/*',
		dest: 'img/'
	}
};

const server = {
	host: 'tandemmarketing.ca',
	user: 'alyssa',
	remotePath: '/var/www/html/sitetesting/chandos18/site/templates/'
};


function styles(){
	return gulp.src(paths.styles.src)
		.pipe(sass({outputStyle: 'compressed'}))
		.pipe(autoprefixer({ browsers: ['last 3 versions'], cascade: false}))
		.pipe(rename({
			basename: 'main',
			suffix:'.min'
		}))
		.pipe(gulp.dest(paths.styles.dest))
		.pipe(sftp({
			host: server.host,
			user: server.user,
			remotePath: server.remotePath+paths.styles.dest
		}));

}

function scripts(){
	return gulp.src(paths.scripts.src)
		.pipe(uglify())
		.pipe(rename({
			suffix:'.min'
		}))
		.pipe(gulp.dest(paths.scripts.dest))
		.pipe(sftp({
			host: server.host,
			user: server.user,
			remotePath: server.remotePath+paths.scripts.dest
		}));
}

function images(){
	return gulp.src(paths.images.src)
		.pipe(imagemin([
				imagemin.gifsicle({interlaced: true}),
				imagemin.jpegtran({progressive: true}),
				imagemin.optipng({optimizationLevel: 6}),
				imagemin.svgo({
					plugins: [
						{removeViewBox: true},
						{cleanupIDs: false},
						{removeUnknownsAndDefaults: false}
					]
				})
			]))
		.pipe(gulp.dest(paths.images.dest))
		.pipe(sftp({
			host: server.host,
			user: server.user,
			remotePath: server.remotePath+paths.images.dest
		}));
}


function watch(){
	gulp.watch(paths.styles.src, styles);
	gulp.watch(paths.scripts.src, scripts);
	gulp.watch(paths.images.src, images);
}

exports.styles = styles;
exports.scripts = scripts;
exports.images = images;
exports.watch = watch;




//const build = gulp.series(gulp.parallel(styles, scripts, images));
