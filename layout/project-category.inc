<div class="container projects">
	<div class="hero text-only">
		<div class="content">
			<h1><?=$page->title?> Projects</h1>
			<div class="intro">
				<?=$page->body?>
			</div>
		</div>	
	</div><!--hero-->
	<div id="main" class="content">
		<article>
			<div class="grid">
				<?php
				foreach($page->children as $child){
				$thumb = $child->images->first->width(500);?>
				<a style="background-image:url('<?=$thumb->url?>');" class="project" data-location="<?=$child->location->name?>" href="<?=$child->url?>">
					
					<div class="info">
						<h2><?=$child->title?></h2>
						<p><?=$child->location->title?></p>
					</div>
					
				</a>
				<?php } ?>
			</div><!--grid-->
			
		</article>
		<div class="sidebar">
			<div class="content">
				<?php include 'inc/social-buttons.php';?>
			</div>
		</div><!--sidebar-->
	</div><!--content-->
</div><!--container-->