<div class="container project-single">
	<div class="hero" style="background-image:url('<?=$page->images->first->url?>');">
		<h1><?=$page->title?></h1>
	</div>
	<div id="main" class="content">
		<article>
			<h2><?=$page->location->title?></h2>
			<?=$page->body?>
			<div class="gallery">
				<?php foreach($page->images as $img){
				$thumb = $img->size(900, 500);?>
					<a class="fancybox" data-fancybox="gallery" data-caption="<?=$img->description?>" href="<?=$img->url?>"><img src="<?=$thumb->url?>"></a>
				<?php }?>
			</div>
			<?php if($page->more_info) echo $page->more_info; ?>
			<p><a href="<?=$pages->get('/projects/')->url?>">« Back to Our Projects</a></p>
		</article>
		<div class="sidebar">
			<div class="content">
				<div class="in-category">
					<?php if($page->featured){ ?>
					<h3><a href="<?=$pages->get('/projects/')->url?>" title="View more Featured Projects by Chandos">Featured</a> /</h3>
					<?php }?>
					<h3><a href="<?=$page->parent->url?>" title="View more <?=$page->parent->title?> Projects by Chandos"><?=$page->parent->title?></a></h3>
				</div>
				<div class='project-info'>

					<?php 
					if(count($page->served_from)){ 
						echo "<h3>Served From</h3><p>";
						foreach($page->served_from as $srv){
						echo "{$srv->title}<br />";
						}
						echo "</p>";
					}

					if($page->sidebar){ echo "{$page->sidebar}"; }

					if(count($page->services)){ 
						echo "<h3>Services</h3><p>";
						foreach($page->services as $srv){
						echo "» {$srv->title}<br />";
						}
						echo "</p>";
					}
					include 'inc/social-buttons.php';?>
				</div>
			</div>
		</div><!--sidebar-->
	</div><!--content-->
	
</div><!--container-->