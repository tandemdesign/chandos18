<div class="container basic">
	<div class="hero big-img" style="background-image:url('<?=$bgImg?>');">
		<h1><span>&nbsp;&nbsp;<?=$page->headline_top?>&nbsp;</span>
			<?php if($page->headline_bottom){?>
			<br />&nbsp;&nbsp;<span><?=$page->headline_bottom?>&nbsp;&nbsp;</span>
			<?php }?>
		</h1>

		<a class="scroll-down" href="#<?=$page->name?>"><svg><use xlink:href="#iconArrow"></use></svg></a>

	</div>
	<div id="<?=$page->name?>" class="content">
		<article>
			<?=$page->body?>
			<div class="logo-grid">
				<?php foreach($page->grid_item as $grid){?>
				<div class="logo-single">
					
					<?php if(count($grid->images)){
					 if($grid->video_url){?>
						<div class="logo-wrap">
						<a target="_blank" href="<?=$grid->video_url?>" title="Visit the <?=$grid->title?> website">
							<img src="<?=$grid->images->first->url?>" alt="<?=$grid->title?> logo"/>
						</a>
						</div>	
						<?php } else { ?>
						<div class="logo-wrap">
							<img src="<?=$grid->images->first->url?>" alt="<?=$grid->title?> logo"/>
						</div>	
						<?php }
					} ?>
					
					
					<div class="info">
						<?php if($grid->video_url){?>
						<h3><a target="_blank" href="<?=$grid->video_url?>" title="Visit the <?=$grid->title?> website"><?=$grid->title?></a></h3>
						<?php } else { ?>
						<h3><?=$grid->title?></h3>
						<?php }?>
						
						<?=$grid->body?>
					</div>					
				</div><!--logo-single-->
				<?php }?>
			</div><!--logo-grid-->
		</article>
		<div class="sidebar">
			<div class="content">
				<?php include 'inc/social-buttons.php';
				$vidTitle;
				if($page->video_url){
				$page->video_title ? $vidTitle = $page->video_title : $vidTitle = "Play Video";	
				if($page->video_thumb){?>
				
				<a class="thumb fancybox" href="<?=$page->video_url?>" title="Play Video <?=$page->video_title?>"><img src="<?=$page->video_thumb->width(1000)->url?>" alt="Video thumbnail"></a>
				<?php } else {?>
				<div class="video-container">
					<div class="btn-wrap">
						<a class="btn fancybox" href="<?=$page->video_url?>" title="Play Video"><svg><use xlink:href="#iconPlay"></use></svg></a>
					</div>
					<h3><?=$vidTitle?></h3>
				</div>
				<?php }
				}?>
			</div><!--content-->

		</div><!--sidebar-->
	</div><!--content-->
</div><!--container-->

