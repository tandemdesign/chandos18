<div class="container">
	<div class="hero big-img" style="background-image:url('<?=$bgImg?>');">
		<h1><span>&nbsp;&nbsp;<?=$page->headline_top?>&nbsp;</span>
			<?php if($page->headline_bottom){?>
			<br />&nbsp;&nbsp;<span><?=$page->headline_bottom?>&nbsp;&nbsp;</span>
			<?php }?>
		</h1>

		<a class="scroll-down" href="#<?=$page->name?>"><svg><use xlink:href="#iconArrow"></use></svg></a>

	</div>
	<div id="<?=$page->name?>" class="content">

		<article>
			<?=$page->body?>
			<?php 
			$locations = $fieldtypes->get('FieldtypeOptions')->getOptions('served_from'); 
			foreach($locations as $location){
				$jobPosts = $pages->find("template=career-single, served_from={$location}");
				if(count($jobPosts)){ 
				echo "<h3>{$location->title}</h3>";
				foreach($jobPosts as $job){?>
				<div class="single">
					<p><strong><?=$job->title?></strong><br /><sub><a href="<?=$job->url?>">Read More »</a></sub></p>
				</div>


				<?php }
				}
			}?>
			
		
			
			
		</article>
		<div class="sidebar">
			<div class="content">
				<?php include 'inc/social-buttons.php';
				$vidTitle;
				if($page->video_url){
				$page->video_title ? $vidTitle = $page->video_title : $vidTitle = "Play Video";?>
				<div class="video-container">
					<div class="btn-wrap">
						<a class="btn fancybox" href="<?=$page->video_url?>"><svg><use xlink:href="#iconPlay"></use></svg></a>
					</div>
					<h3><?=$vidTitle?></h3>
				</div>
				<?php }?>
			</div>

		</div><!--sidebar-->
	</div><!--content-->
</div><!--container-->

