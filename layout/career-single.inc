<div class="container">
	<div class="hero text-only">
		<div class="content">

			<h1>Careers</h1>

		</div>
		
	</div>
	<div id="<?=$page->name?>" class="content">

		<article>
			<h2><?=$page->title?></h2>

			<h3>Location: <?php foreach($page->served_from as $loc){ echo "{$loc->title} "; }?></h3>
			<p><a href="<?=$page->parent->url?>">« Back to Careers</a></p>
			<?=$page->body?>
		</article>
		<div class="sidebar">
			<div class="content">
				
				<?php include 'inc/social-buttons.php';?>

			</div>

		</div><!--sidebar-->
	</div><!--content-->
</div><!--container-->

