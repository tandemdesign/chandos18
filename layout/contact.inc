<div class="container contact">
	<div class="hero">
		<div class="content">
			<h1>We're ready to show  you how construction should be done</h1>
		</div>

		<div class="map-container content">
			<?php include 'img/contact-map.svg';?>
			
		</div>
	</div>
	
	<div class="content">
		<article>
		<?php foreach($page->office as $office){
		$id = strtolower(str_replace(" ","-", $office->title));
		$thumb = $office->images->first->size(625,440)->url; ?>	
		<div class="contact-single">
			<a data-fancybox data-src="#<?=$id?>" href="javascript:;">
				<img class="thumb" src="<?=$thumb?>" alt="Chandos <?=$office->title?> Office"/>
				<?=$office->title?>

			</a>
		</div>
		<?php }?>
		</article>
	</div>

	<?php foreach($page->office as $office){
	$id = strtolower(str_replace(" ","-", $office->title));?>
	<div id="<?=$id?>" class="contact-popup" style="display:none">
		<div class="info">
			<?php include 'img/contact-map.svg';?>
			<h3><?=$office->title?></h3>
			<?=$office->body?>
			
		</div><!--info-->
		
		<div class="bg-office" style="background-image:url('<?=$office->images->first->url?>');"></div>
	</div>
	<?php }?>
	

</div><!--container-->