<div class="container">
	<div class="hero text-only">
		<div class="content">

			<h1><?=$page->title?></h1>

		</div>
		
	</div>
	<div id="<?=$page->name?>" class="content">

		<article>
			<?=$content?>
			
			
			
		</article>
		<div class="sidebar">
			<div class="content">
				<?php include 'inc/social-buttons.php';
				$vidTitle;
				if($page->video_url){
				$page->video_title ? $vidTitle = $page->video_title : $vidTitle = "Play Video";	
				if($page->video_thumb){?>
				
				<a class="thumb fancybox" href="<?=$page->video_url?>" title="Play Video <?=$page->video_title?>"><img src="<?=$page->video_thumb->width(1000)->url?>" alt="Video thumbnail"></a>
				<?php } else {?>
				<div class="video-container">
					<div class="btn-wrap">
						<a class="btn fancybox" href="<?=$page->video_url?>" title="Play Video"><svg><use xlink:href="#iconPlay"></use></svg></a>
					</div>
					<h3><?=$vidTitle?></h3>
				</div>
				<?php }
				}?>
			</div><!--content-->

		</div><!--sidebar-->
	</div><!--content-->
</div><!--container-->

