<!-- press releases -->
<div class="container">
	<div class="hero text-only">
		<div class="content">

			<h1><?=$page->title?></h1>

		</div>
		
	</div>
	<div id="<?=$page->name?>" class="content">

		<article>
			<?=$page->body?>
			<div class="basic-grid">
				<?php foreach($page->grid_item as $item){
				$img;
				$bg = $item->images->first ? 'url('.$item->images->first->url.');' : '#797872;' ?>
				<div class="grid-item">
					<?php if($item->images->first) { echo "<img src='{$item->images->first->url}'>" ;}?>
					<div class="info">
						<h3><?=$item->title?></h3>
						<?=$item->body?>
						<?php if(count($item->file)){
						echo "<p class='download'><a href='{$item->file->first->url}'>Download PDF</a></p>";
						}?>
					</div><!--info-->
				</div><!--grid-item-->
				<?php }?>
			</div><!--grid-->
			
			
		</article>
		<div class="sidebar">
			<div class="content">
				<?php include 'inc/social-buttons.php';
				$vidTitle;
				if($page->video_url){
				$page->video_title ? $vidTitle = $page->video_title : $vidTitle = "Play Video";?>
				<div class="video-container">
					<div class="btn-wrap">
						<a class="btn fancybox" href="<?=$page->video_url?>"><svg><use xlink:href="#iconPlay"></use></svg></a>
					</div>
					<h3><?=$vidTitle?></h3>
				</div>
				<?php }?>
			</div>

		</div><!--sidebar-->
	</div><!--content-->
</div><!--container-->

