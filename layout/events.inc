<div class="container">
	<div class="hero big-img" style="background-image:url('<?=$bgImg?>');">
		<h1><span>&nbsp;&nbsp;<?=$page->headline_top?>&nbsp;</span>
			<?php if($page->headline_bottom){?>
			<br />&nbsp;&nbsp;<span><?=$page->headline_bottom?>&nbsp;&nbsp;</span>
			<?php }?>
		</h1>

		<a class="scroll-down" href="#<?=$page->name?>"><svg><use xlink:href="#iconArrow"></use></svg></a>

	</div>
	<div id="<?=$page->name?>" class="content">

		<article>
			<?=$page->body?>

			<?php 
			if(count($page->children)){
			foreach($page->children as $event){
			count($event->images) ? $thumb = $event->images->first->width(900) : $thumb='';?>
			<div class="single event">
				<?php if($thumb){?><img class="thumb" src="<?=$thumb->url?>"><?php }?>
				<div class="info">
					<h3><?=$event->title?></h3>
					<p><strong><?=$event->headline_top?></strong><br />
					<strong>Starts:</strong> <?=$event->start_date?> <?=$event->start_time?>
					<?php if($event->end_time && !$event->end_date){ 
					echo "- {$event->end_time}";
					} else if ($event->end_date){
					echo "<br /><strong>Ends:</strong> {$event->end_date} {$event->end_time}";
					}?>
					
						
					</p>
					<?=$event->body?>
				</div><!--info-->
			</div>	

			<?php } 
			} else { echo "<p><strong>Check back soon for more events!</strong></p>"; } 	?>
			
		
			
			
		</article>
		<div class="sidebar">
			<div class="content">
				<?php include 'inc/social-buttons.php';?>
			</div>

		</div><!--sidebar-->
	</div><!--content-->
</div><!--container-->

