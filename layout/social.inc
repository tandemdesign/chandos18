<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="container social-media">
	<div class="hero text-only">
		<div class="content">

			<h1>Social Media</h1>
			<?php include 'inc/social-buttons.php';?>
		</div>
		
	</div>

	<div id="<?=$page->name?>" class="content">
		
		<article>
			
			<div class="social-single">
				<h2>Twitter <span><a href="https://twitter.com/chandosltd?ref_src=twsrc%5Etfw">Follow us »</a></span></h2>
				<a class="twitter-timeline" data-tweet-limit="3" data-theme="light" data-height="800" data-width="700" data-link-color="#ea0029" data-dnt="true" data-chrome="" href="https://twitter.com/ChandosLTD?ref_src=twsrc%5Etfw">Tweets by ChandosLTD</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script> 
			</div>
			<div class="social-single">
				<h2>Facebook <span><a href="https://www.facebook.com/ChandosConstruction/">Like us »</a></span></h2>
				<div class="fb-page" data-href="https://www.facebook.com/ChandosConstruction/" data-tabs="timeline" data-width="500" data-height="2000" data-small-header="true" data-adapt-container-width="true" data-hide-cover="true" data-show-facepile="false"><blockquote cite="https://www.facebook.com/ChandosConstruction/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/ChandosConstruction/">Chandos Construction</a></blockquote></div>
			</div>
		</article>
		
	</div><!--content-->
</div><!--container-->

