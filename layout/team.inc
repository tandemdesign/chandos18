<div id="teamPage" class="container team">
	<div class="hero text-only">
		<div class="content">

			<h1><?=$page->title?></h1>
			<label class="search-open" for="search"><p>Filter</p> <svg class="search-icon"><use xlink:href="#iconSearch"></use></svg></label>
		</div>
		<?php include 'inc/search-team.php';?>
	</div>

	
	<div class="content">
		<article>
			<?=$page->body?>
			<div class="team-category results">
			

			<?php 
			$members = $page->team_member->sort(title);
			foreach($members as $member){
			$member->images->first ? $thumb = $member->images->first->size(400,550)->url : $thumb = $templates."img/thumb-people.png"; 
			$class = '';
			$firstCh = $member->title[0];
			foreach($member->department as $dept){
				$class .= $dept->value . ' '; 
			}?>
				<a class="team-single <?=$class?>" data-fancybox data-src="#<?=strtolower($firstCh.'-'.$member->headline_bottom)?>" href="javascript:;">
					<img class="thumb" src="<?=$thumb?>"><br />
					<?=$member->title?> <?=$member->headline_bottom?>
				</a><!--team member-->
				
			
			<?php }?>
			<p class="search-error">Sorry, your search returned no results.</p>
			</div><!--category-->
		</article>
	</div><!--content-->
</div><!--container-->


<?php 

foreach($members as $member){
$firstCh = $member->title[0];?>
<div id="<?=strtolower($firstCh.'-'.$member->headline_bottom)?>" class="team-info bgimg" style="display:none" >
	<h3><?=$member->title?> <?=$member->headline_bottom?></h3>
	
	<div class="content">
		<div class="portrait">
			<img src="<?=$member->images->first->url?>">
			<?php if($member->headline_top){?><a href="<?=$member->headline_top?>" target="_blank"><svg class="icon"><use xlink:href="#iconLinkedIn"></use></svg></a><?php }?>
		</div>
		<div class="bio">
			<?=$member->body?>
		</div>
	</div>
	<div class="bg-img" style="background-image:url('<?=$member->bg_image->url?>"></div>
</div>
<?php } ?>
