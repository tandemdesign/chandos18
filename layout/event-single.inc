<div class="container">
	<div class="hero text-only">
		<div class="content">

			<img src="<?=$page->images->first->url?>"/>

		</div>
		
	</div>
	<div id="<?=$page->name?>" class="content">

		<article>
			<h2><?=$page->title?></h2>
			<h3>Location: <?=$page->headline_top?></h3>
			<p><strong>Starts:</strong> <?=$page->start_date?> <?=$page->start_time?>
			<?php if($page->end_time && !$page->end_date){ 
			echo "- {$page->end_time}";
			} else if ($page->end_date){
			echo "<br /><strong>Ends:</strong> {$page->end_date} {$page->end_time}";
			}?>
			
				
			</p>
			
			
			<?=$page->body?>
			
			
			
		</article>
		<div class="sidebar">
			<div class="content">
				<p style="float:right; margin-right:1em;"><a href="<?=$page->parent->url?>">« Back to Events</a></p>
				<?php include 'inc/social-buttons.php';?>

			</div>

		</div><!--sidebar-->
	</div><!--content-->
</div><!--container-->

