<?php
	$projectPages = $pages->find('template=project-single, sort=title');
?>

<div class="container projects">
	<div class="hero text-only">
		<div class="content">
			
			<h1><?=$page->headline_top?></h1>
			<label class="search-open" for="search"><p>See <?=count($projectPages)?> More</p> <svg class="search-icon"><use xlink:href="#iconSearch"></use></svg></label>
		</div>
		<?php include 'inc/search-projects.php';?>
	</div><!--heroo-->

	<div class="content">
		
		<div class="intro">
			<?=$page->body?>
			<p class="filtered"></p>
		</div>
	</div>	
	<div id="main" class="content">
		
		<article>
			<div class="grid results filtered">
			<?php foreach($projectPages as $project){
				$thumb = $project->images->first->width(500);
				$servedFrom = '';
				$services = '';
				foreach($project->served_from as $s){
					$title = strtolower($s->title);
					$name = preg_replace("/[\s_]/", "-", $title);
					$servedFrom .= " {$name}";
				}
				foreach($project->services as $service){
					$services .= " serv{$service->id}";
				}
				$project->featured ? $featured = 'featured' : $featured = ''; 
				$project->featured ? $class = 'active' : $class = '';?>
 				<a style="background-image:url('<?=$thumb->url?>');" class="project <?=$featured?> <?=$class?> <?=strtolower($servedFrom)?> <?=$services?> <?=$project->parent->name?>" href="<?=$project->url?>">
					<div class="info">
						<h2><?=$project->title?></h2>
						<p><?=$project->location->title?></p>
					</div>
					
				</a>
			<?php }?>
			</div>
			<p class="search-error">Sorry, your search returned no results.</p>
		</article>
		<div class="sidebar">
			<div class="content">
				<?php include 'inc/social-buttons.php';?>
			</div>
		</div><!--sidebar-->
	</div><!--content-->
</div><!--container-->


