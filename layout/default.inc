<div class="container basic">
	<div class="hero big-img" style="background-image:url('<?=$bgImg?>');">
		<h1><span>&nbsp;&nbsp;<?=$page->headline_top?>&nbsp;</span>
			<?php if($page->headline_bottom){?>
			<br />&nbsp;&nbsp;<span><?=$page->headline_bottom?>&nbsp;&nbsp;</span>
			<?php }?>
		</h1>

		<a class="scroll-down" href="#<?=$page->name?>"><svg><use xlink:href="#iconArrow"></use></svg></a>

	</div>
	<div id="<?=$page->name?>" class="content">
		<article>
			<?=$page->body?>
			<?php if($page->add_form){
				include "inc/form.php";
			}?>
		
		</article>
		<div class="sidebar">
			<div class="content">
				<?php if($page->sidebar){?>
				<div class="project-info">
					<?=$page->sidebar?>
				</div>
				<?php }?>
				
				<?php include 'inc/social-buttons.php';
				$vidTitle;
				if($page->video_url){
				$page->video_title ? $vidTitle = $page->video_title : $vidTitle = "Play Video";	
				if($page->video_thumb){?>
				
				<a class="thumb fancybox" href="<?=$page->video_url?>" title="Play Video <?=$page->video_title?>"><img src="<?=$page->video_thumb->width(1000)->url?>" alt="Video thumbnail"></a>
				<?php } else {?>
				<div class="video-container">
					<div class="btn-wrap">
						<a class="btn fancybox" href="<?=$page->video_url?>" title="Play Video"><svg><use xlink:href="#iconPlay"></use></svg></a>
					</div>
					<h3><?=$vidTitle?></h3>
				</div>
				<?php }
				}?>
			</div><!--content-->

		</div><!--sidebar-->
	</div><!--content-->
</div><!--container-->