<div class="container parent">
	<div class="hero text-only">
		<div class="content">

			<h1><?=$page->title?></h1>

		</div>
		
	</div>
	<div id="<?=$page->name?>" class="content">
		<article>
			<?=$page->body?>
			<div class="basic-grid">
			<?php foreach($page->children as $child){
			$child->bg_image ? $thumb = $child->bg_image->size(700,466)->url : $thumb = $templates."img/thumb.jpg";?>
				<div class="grid-item">
					<img src="<?=$thumb?>" />
					<h3><a href="<?=$child->url?>" title="Visit The Chandos <?=$child->title?> page"><?=$child->title?> »</a></h3>
				</div>
			<?php }?>
			</div>
		</article>
		<div class="sidebar">
			<div class="content">
				<?php include 'inc/social-buttons.php';
				$vidTitle;
				if($page->video_url){
				$page->video_title ? $vidTitle = $page->video_title : $vidTitle = "Play Video";?>
				<div class="video-container">
					<div class="btn-wrap">
						<a class="btn fancybox" href="<?=$page->video_url?>"><svg><use xlink:href="#iconPlay"></use></svg></a>
					</div>
					<h3><?=$vidTitle?></h3>
				</div>
				<?php }?>
			</div>

		</div><!--sidebar-->
	</div><!--content-->
</div><!--container-->