#Chandos 2019 Website 
*chandos.com*

*sitetesting/chandos18*

This site was made in [Processwire](http://processwire.com). This is the /templates/ directory. 

[Processwire Readme](https://github.com/processwire/processwire/blob/master/README.md)

To log into admin panel - /processwire

Client has access via usernames chandos-careers, chandos-projects, chandos-user.

##Installing
```
yarn
```

##Template structure

This site uses the Delayed Output strategy. 

1. The initialization file is loaded (\_init.php). We use it to define placeholder variables for content regions.
2. The template file is loaded (i.e. home.php or another).
     We use it to populate values into the placeholder variables. 
3. The main output file is loaded (\_main.php). It is an HTML document that outputs the placeholder variables.

[How Do Templates Work](https://processwire.com/docs/start/templates)

The template names might be a little confusing because so much changed and the project was pretty rushed.  

| Template      | Description |
| ------------- | ----------- |
| basic-grid    | Used for Media page (/press-releases/). Contains "Grid Item" repeater that links to either a file or URL. Default image is PDF icon. |
| basic-page    | Basic page with huge hero image. Used for a bunch of pages (example: /who-we-are/governance/). Has 2 inputs to put headline to create stacked effect. Only use top if just one line is needed. |
| career-single | Job posting template. |
| careers       | Main careers page - contains list of jobs sorted by city. |
| contact       | Contact page. Offices are repeaters. New cities on map needs to be manually added via code (is SVG) |
| event-single  | Event posting. |
| events        | Main events page - contains list of events sorted by date. |
| home          | Home page template. |
| logo-grid     | Basic page with logo grid. Example is /who-we-are/partnerships/. Repeater that contains image, title, optional body/description and optional link to file or URL. |
| parent        | Parent page for navigation purposes. Just has list of children. |
| plain-title   | Basic page with no hero image. Title slides in. Example: /who-we-are/overview/ |
| project-category | Projects are organized by "marketing category" eg. Industrial, Retail, etc. |
| project-single | Project page |
| projects       | All projects displayed on this page. Defaults to only "featured" projects. |
| sitemap        | XML site map for SEO. Automatically generated. |
| social         | Currently not in use. Just social media feeds. |
| team           | People/staff listing. Staff are repeaters not individual pages. |





