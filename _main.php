<?php namespace ProcessWire;
if(!$useMain) return; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
	<title><?=$title?> | Chandos Construction</title>
	<meta name="description" content="<?=$description?>">
	<link rel="apple-touch-icon" sizes="180x180" href="<?=$templates?>/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?=$templates?>/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?=$templates?>/favicon-16x16.png">
	<link rel="manifest" href="<?=$templates?>/site.webmanifest">
	<link rel="mask-icon" href="<?=$templates?>/safari-pinned-tab.svg" color="#ea0029">
	<meta name="msapplication-TileColor" content="#ea0029">
	<meta name="theme-color" content="#ffffff">
	<meta property="og:image" content="<?=$templates?>img/og-image_event-iwp2019.jpg">
	<meta property="og:image" content="<?=$templates?>img/og-image.jpg">	
	<meta property="og:image" content="<?=$page->images->first->url?>">
	<meta property="og:image:width" content="1800">
	<meta property="og:image:height" content="942">
	<meta property="og:title" content="Chandos Construction">
	<meta property="og:description" content="<?=$home->summary?>">
	<meta property="og:url" content="chandos.com">
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@ChandosLTD">
	<meta name="twitter:title" content="<?=$title?> | Chandos Construction">
	<meta name="twitter:description" content="<?=$description?>">
	<meta name="twitter:image" content="<?=$templates?>img/og-image.jpg">
	<link rel="stylesheet" href="<?=$templates?>css/main.min.css">
</head>
<?php include('inc/tracking.php');?>
<body>
	<?php include 'svg/icons.svg';?>
	<header>
		<a href="<?=$homepage->url?>"><img class="logo" src="<?=$templates?>img/logo-chandos.svg" alt="Chandos Logo"></a>
		
		<nav class="main">
			<ul>
				<li class="has-subnav"><a href="#">Who We Are</a>
					<ul class="subnav">
						<?php foreach($pages->get('/who-we-are/')->children as $sub){
						echo "<li><a href='{$sub->url}'>{$sub->title}</a></li>";
						}?>
					</ul>
				</li>
				<li class="has-subnav"><a href="#">What We Do</a>
					<ul class="subnav">
						<?php foreach($pages->get('/what-we-do/')->children as $sub){
						echo "<li><a href='{$sub->url}'>{$sub->title}</a></li>";
						}?>
					</ul>
				</li>
				<li class="has-subnav"><a href="#">Expertise</a>
					<ul class="subnav">
						<?php foreach($pages->get('/expertise/')->children as $sub){
						echo "<li><a href='{$sub->url}'>{$sub->title}</a></li>";
						}?>
					</ul>
				</li>
				<li><a href="<?=$pages->get('/projects/')->url?>">Our Projects</a></li>
				<li class="has-subnav"><a href="#">News</a>
					<ul class="subnav">
						<li><a href="https://vimeo.com/channels/seethingsdifferently" target="_blank">Video</a></li>
						<?php foreach($pages->get('/news/')->children as $sub){
						echo "<li><a href='{$sub->url}'>{$sub->title}</a></li>";
						}?>
						
					</ul>
				</li>
				<li><a href="<?=$pages->get('/careers/')->url?>" title="Careers at Chandos">Careers</a></li>
				<li><a href="<?=$pages->get('/contact/')->url?>" title="Contact Us">Contact</a></li>


			</ul>
		</nav>
		
	</header>
	<a class="menu-icon" href="#">
		<div class="icon-wrap">
			<div class="icon">
			</div>
		</div>
	</a>
	<div class="menu-overlay">
		<a href="<?=$homepage->url?>"><img class="small-logo" src="<?=$templates?>img/logo-chandos_small.svg"></a>
	</div>

	
	<?php
	include "layout/{$layout}.inc"; ?>
	

	<?php if($layout != 'home'){?>
	<footer>
		<div class="content">
			<div>
				<?php include 'inc/social-buttons.php';?>
			</div>
			<div class="nav-container">
				
				<nav>
					<ul>
						<li><a href="<?=$pages->get('/projects/')->url?>">Projects</a></li>
						<li><a href="<?=$pages->get('/contact/')->url?>">Contact</a></li>
						<li><a href="<?=$pages->get('/who-we-are/careers/')->url?>">Careers</a></li>
						<li><a title="Login to Building Connected" href="https://app.buildingconnected.com/create-account" target="_blank">Building Connected</a></li>
					</ul>
				</nav>
			</div>
			<div>
				
			</div>
		</div>
	</footer>
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="<?=$templates?>js/fancybox.min.js"></script>
	<script src="<?=$templates?>js/main.min.js"></script>


</body>
</html>
<?php }?>

